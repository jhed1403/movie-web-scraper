from selenium import webdriver
import time
import itertools
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

# TODO:
# make the height of the page argument
# make the scroll pause time argument
# add the ability to set a range of year to scrape

# example option: add 'incognito' command line arg to options
option = webdriver.ChromeOptions()
option.add_argument("--incognito")

# create new instance of chrome in incognito mode
browser = webdriver.Chrome(executable_path='/home/factolej/movie-web-scraper/chromedriver')

# go to website 
browser.get("https://www.cinesift.com/#/")

SCROLL_PAUSE_TIME = 3
# Get scroll height
last_height = browser.execute_script("return document.body.scrollHeight")
while True:
    # Scroll down to bottom
    browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Wait to load page
    time.sleep(SCROLL_PAUSE_TIME)

    # Calculate new scroll height and compare with last scroll height
    new_height = browser.execute_script("return document.body.scrollHeight")
    if new_height > 4000:
        break
    last_height = new_height
    print last_height

# get all of the movie titles
titles_element = browser.find_elements_by_xpath("//div[@class='filmInfoContainer']//div[@class='title ng-binding']")
titles = []
for x in titles_element:
	if len(x.text) > 0:
		titles.append(x.text)

# get all of the year values
years_element = browser.find_elements_by_xpath("//div[@class='filmInfoContainer']//div[@class='film-section ng-binding']")
years = []
for x in years_element:
	if len(x.text) > 0:
		date_year = str(x.text)
		date_year = date_year.replace("(", "")
		date_year = date_year.replace(")", "")
		years.append(date_year)

# get all the genre
genres_element = browser.find_elements_by_xpath("//div[@class='film-section genre ng-binding']")
genres = []
for x in genres_element:
	if len(x.text) > 0:
		genres.append(x.text)

# get all the directors
#directors_element = browser.find_elements_by_xpath("//a[@class='ng-binding']")
directors_element = browser.find_elements_by_xpath("//a[@ng-click='searchFilmsByDirector(director)']")
directors = []
for x in directors_element:
	if len(x.text) > 0:
		directors.append(x.text)

# get all the reviewers
reviewers_element = browser.find_elements_by_xpath("//div[@class='film-section']//div[@class='rating-bar-meat']")
reviewers = []
for x in reviewers_element:
	if len(x.text) > 0:
		reviewers.append(x.text)

# get all the reviews		
review_element = browser.find_elements_by_xpath("//div[@class='film-section']//div[@class='rating-bar-score ng-binding']")
ratings = []
for x in review_element:
	if len(x.text) > 0:
		ratings.append(x.text)

# combine reviewers and reviews
movie_review = []
for reviewer, rating in zip(reviewers, ratings):
    movie_review.append(reviewer + ':' + rating)

# regroup reviews
movie_ratings = []
for x in movie_review:
    if x.startswith('Tomatometer:'):
        movie_ratings.append([])
    if not x.startswith('Metascore:'):
    	movie_ratings[-1].append(x)

# get availability
#availability_element = browser.find_elements_by_xpath("//div[@class='film-section availableOn']//*")
availability_element = browser.find_elements_by_xpath("//div[@class='filmInfoContainer']//*")
availability = []
for x in availability_element:
	if len(x.text) > 0:
		if x.text=='Amazon Video' or x.text=='Blu-ray' or x.text=='Netflix':
			availability.append(x.text)

movie_availability = []
for x in range(len(availability)): 
	if availability[x]==availability[x-1]:
		movie_availability.append([])
	if availability[x].startswith('Amazon'):
			movie_availability.append([])
	movie_availability[-1].append(availability[x])	

rates = []
for sublst in movie_ratings:
	r = ''
	for item in sublst:
		r = r + ' ' + str(item,)
	if r.startswith(" "): 
		r = r[1:]
	rates.append(r)
              
platforms = []
for sublst in movie_availability:
	p = ''
	for item in sublst:
		p = p + ' ' + str(item,)     
	if p.startswith(" "): 
		p = p[1:]
	platforms.append(p)

f= open("movie.csv","w+")
result = ""
# pair each title with its corresponding info using zip function and print them
for title, year, genre, director, rate, platform in zip(titles, years, genres, directors, rates, platforms):
    # print(title + ',' + year + ',' + genre + ',' + director + ',' + rate + ',' + platform)
    result = result + title + ',' + year + ',' + genre + ',' + director + ',' + rate + ',' + platform + '\n'
print result.encode('utf-8')
f.write(result.encode('utf-8'))
f.close()