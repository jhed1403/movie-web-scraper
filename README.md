## movie-web-scraper

A simple web scraper that scrapes movie information from a dynamic website, https://www.cinesift.com/.
The output file is a csv file.

## Dependencies

* Selenium
* Chromedriver